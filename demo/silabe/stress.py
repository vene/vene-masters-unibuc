# -*- encoding: utf8 -*-

import fileinput
import subprocess
import sys

from StringIO import StringIO

import numpy as np

ACCENT = u'\u0301'

def crfsuite_feature_names(size=4, negative=False):
    indices = range(-size, 0) if negative else range(1, size + 1)
    indices = map(str, indices)
    return '\t'.join(['c[%s]=%%s' % ''.join(indices[i:(i + k + 1)])
                      for k in range(size)
                      for i in range(size - k)])


def _char_ngrams(s, size=4):
    s_len = len(s)
    ngrams = []
    for n in xrange(1, min(size + 1, s_len + 1)):
        for i in xrange(s_len - n + 1):
            ngrams.append(s[i: i + n])
    return ngrams


def _build_feature_dict(s, position, size, ngram_size):
    features = {}
    l = len(s)
    for left in xrange(-size, size + 2):
        for sz in xrange(0, min(ngram_size, size - left + 1)):
            if position + left >= 0 and position + left + sz + 1 <= l:
                features['%s|%s' % (left, left + sz)] =\
                    s[position + left:position + left + sz + 1]
    return features


def hyphen_features(word, size, left_tpl, right_tpl):
    res = StringIO()    
    for k in xrange(1, len(word)):
        left, right = word[:k], word[k:]
        left_size = min(len(left), size)
        right_size = min(len(right), size)
        print >> res, '%s\t%s' % (
            left_tpl[left_size - 1] % tuple(_char_ngrams(left[-size:], size)),
            right_tpl[right_size - 1] % tuple(_char_ngrams(right[:size], size)))
    return res.getvalue()


def get_split_features(word, before_split):
    after_split = [False] + before_split[:-1]
    syll_from_left = np.cumsum(after_split)
    syll_from_right = np.cumsum(before_split[::-1])[::-1]
    fst = syll_from_left == 0
    snd = syll_from_left == 1
    lst = syll_from_right == 0
    pen = syll_from_right == 1
    return before_split, after_split, fst, snd, lst, pen


def stress_features(word, hyphens, size=2):
    res = StringIO()    
    before, after, first, second, last, penultimate = get_split_features(word, hyphens)
    for (k, bf, af, fst, snd, lst, penult) in zip(xrange(len(word)), before, after, first, second, last, penultimate):
        print >> res, 'bf=%d\taf=%d\tfst=%d\tsnd=%d\tlast=%d\tpenult=%d\t%s' % (
            bf,
            af,
            fst,
            snd,
            lst,
            penult,
            '\t'.join("c[%s]=%s" % item
                      for item
                      in _build_feature_dict(word, k, 2, 2).items()))
    return res.getvalue()


if __name__ == '__main__':
    N = 4  # n-gram size
    left_tpl = [crfsuite_feature_names(k, True) for k in xrange(1, N + 1)]
    right_tpl = [crfsuite_feature_names(k, True) for k in xrange(1, N + 1)]

    for word in fileinput.input(openhook=fileinput.hook_encoded("utf-8")):
        word = word.decode('utf-8').strip().lower()
        features = hyphen_features(word.strip().lower(),
                                   size=N,
                                   left_tpl=left_tpl,
                                   right_tpl=right_tpl)
        p = subprocess.Popen(['crfsuite', 'tag', '-m', 'models/4grams.C=1.0.nb.model'],
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        hyp_tags = p.communicate(features.encode('utf-8'))[0].split()
        hyphens = [tag == '0' for tag in hyp_tags] + [False]
        show_hyp = [{'0': '-'}.get(tag, '') for tag in hyp_tags] + ['']
        features = stress_features(word, hyphens, size=2)
        p = subprocess.Popen(['crfsuite', 'tag', '-m', 'models/syl_50_2.model'],
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        stress_tags = p.communicate(features.encode('utf-8'))[0].split()
        show_stress = [{'1': ACCENT}.get(tag, '') for tag in stress_tags]
        print ''.join(ch for pair in zip(word, show_stress, show_hyp)
                      for ch in pair)

