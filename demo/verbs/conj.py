import sys
import subprocess
from time import time

from crfsuite_utils import crfsuite_features

if __name__ == '__main__':
    for arg in sys.argv[2:]:
        word = arg.decode('utf-8').strip()
        features = crfsuite_features([(word[:k], word[k], word[k + 1:], None)
                                      for k in xrange(len(word))], size=4)
        t0 = time()
        p = subprocess.Popen(['crfsuite', 'tag', '-m', sys.argv[1]],
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        tags = p.communicate(features.encode('utf-8'))[0].split()
        print >> sys.stderr, "Tagging took %.4fs" % (time() - t0)
        tpl = u'{:3s}' * len(word)
        print tpl.format(*tags)
        print tpl.format(*word)
        print
