# Învățăre structurată și secvențială.  Aplicații în procesarea limbajului natural.

## Abstract (provizoriu)

În formularea cea mai simplificată a învățării automate, obiectivul este
predicția unei variabile dependente Y, de exemplu o variabilă de tip
adevărat/fals (clasificare binară).  Recent, puterea de calcul și dezvoltarea
cercetării în algoritmi de învățare a permis formularea și rezolvarea directă
a unor sarcini de învățare unde Y aparține unui spațiu structurat.

Clasificarea binară sau cu clase multiple denotă absența structurii.  Regresia
corespunde unui spațiu structurat ca axa numerelor reale, cu relația de ordine.
Mai departe, întâlnim, printre altele: problema clasării (ranking), unde
clasele au o relație de ordine dar nu au noțiunea de distanță.  Problema
etichetării secvențiale, unde pentru o instanță trebuie etichetate mai multe
obiecte, în mod dependent.  Problema învățării de arbori, ca de exemplu în
prezicerea arborilor de parsare sintactici sau de dependență.

În lucrarea prezentă formulăm problema învățării structurate și exemplele
prezentate mai sus și prezentăm literatura pe tema metodelor actuale de
modelare și rezolvare a problemelor de structurate.  Ne concentrăm pe
problema învățării secvențiale, unde comparăm eficiența și flexibilitatea
implementărilor curente, și prezentăm aplicații în procesarea limbajului
natural: una comună (etichetarea părtilor de vorbire) și una inedită,
etichetarea morfologică a verbelor.
