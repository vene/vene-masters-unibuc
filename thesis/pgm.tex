\chapter{Modele probabiliste grafice}
Un mod convenabil de abordare a multor probleme de învățare automată și
de modelare statistică sunt date de așa-numitele modele grafice introduse
de~\citet{pearl}.  Prin utilizarea puterii de expresie a teoriei grafurilor,
acest cadru de modelare permite concizie și claritate mai mare, fapt ce e
posibil să fi avut un rol în ușurarea progresului în alte discipline în care
modelele au fost folosite.

Scopul modelelor grafice este de a reprezenta interacțiile dintre variabilelele
aleatoare interdependente ce descriu sistemul modelat.  Noutatea lor consta în
accentul pus pe raritate, prin limitarea numărului de parametri necesari pentru
specificarea întregului model.

Modelele probabiliste grafice au la bază două tipuri de modele: rețelele
Bayes, bazate pe grafuri orientate aciclice, și rețelele Markov, bazate pe
grafuri neorientate.
\section{Rețele Bayes}
Rețelele Bayes sau rețelele de încredere (Bayes networks, belief networks)
sunt grafuri orientate aciclice în care nodurile reprezintă variabile
aleatoare, iar arcele reprezintă dependențe între ele.  
Densitatea de repartiție a unui sistem complex cu $n$ variabile este:
$$P(X_1, X_2, ..., X_n)$$
Dacă presupunem că variabilele sunt discrete și pot lua $d$ valori fiecare,
atunci pentru parametrizarea acestei distribuții sunt necesare $d^n - 1$
valori.  Cu cât sistemul este mai complex, cu atât mai mulți parametri
sunt necesari, iar estimarea eficientă a parametrilor necesită cantităti
masive de date~\citep{pgm}.  În majoritatea cazurilor însă, variabilele
sistemului nu se influențează direct toate între ele, ci există
așa-numitele presupuneri de independență condițională și necondițională.
Figura \ref{fig:param} arată cât de drastic se poate reduce dimensionalitatea
unui model făcând doar câteva presupuneri de independență.

Un model grafic extrem de important este modelul Bayes naiv~\ref{fig:nb}.
Modelul descrie un număr de variabile măsurabile și o clasă ce trebuie prezisă,
ce se presupune că influențează în mod cauzal comportamentul variabilelor.  De
exemplu, în diagnoza medicală, variabila-clasă ce ar trebui prezisă este
prezența sau absența unei anumite boli, iar varibilele observabile sunt
simptomele.  Sub presupunerea (mai degrabă falsă) că simptomele sunt
independente, reprezentarea modelului se poate face extrem de compact, trebuind
doar estimate probabilitățile locale $P(X_i|Y)$.  Din factorizarea
corespunzătoare figurii, se obține:
$$P(X_1, ..., X_n, Y) = P(Y) \prod_i P(X_i|Y)$$

Derivarea regulii de clasificare se face aplicând teorema lui Bayes, de unde
și numele modelului:
$$
P(Y=y|X_1=x_1, ..., X_n=x_n) = \frac{P(X_1=x_1, ..., X_n=x_n|Y=y)}
{\sum_{y'\in\mathcal{Y}} P(X_1=x_1, ..., X_n=x_n|Y=y')}
$$

\begin{figure}[b]
\centering
\input{img/nb.tex}
\caption{Modelul grafic asociat clasificatorului bayesian naiv.}
\label{fig:nb}
\end{figure}

\begin{figure}
\centering
\input{img/lr.tex}
\caption{Modelul grafic corespunzător regresiei logistice.}
\label{fig:lr}
\end{figure}
\begin{figure}[b]
\centering
\begin{subfigure}[b]{0.4\linewidth}
\centering
\input{img/all_connected.tex}
\caption{$P(A,B,C,D) = P(A) P(B|A) P(C|A, B) P(D|A, B, C)$.  Patru
variabile complet conectate. Orice distribuție posibilă peste ele 
poate fi modelată de această parametrizare.}
\label{fig:allconn}
\end{subfigure}
\begin{subfigure}[b]{0.4\linewidth}
\centering
\input{img/some_connected.tex}
\caption{$P(A, B, C, D) = P(A) P(B|A) P(C|A) P(D|B, C)$. O
distribuție peste patru variabile aleatoare cu multiple
presupuneri de independență condițională.}
\label{fig:someconn}
\end{subfigure}
\caption{Raritate în parametrizare prin presupuneri de independență condițională.}
\label{fig:param}
\end{figure}

\section{Câmpuri Markov Aleatoare (MRF)}
Câmpurile Markov aleatoare (MRF), sau rețele Markov, sunt modele grafice
neorientate.  În consecință, muchille indică dependență directă
reciprocă între variabile.  Un caz particular și util de rețele Markov
sunt rețelele Markov de perechi (pairwise Markov network), unde
distribuția de probabilitate modelată se descompune în interacțiuni
binare între câte două noduri.  Spre deosebire de rețelele Bayes,
rețelele Markov nu impun o descompunere în densități de probabilitate
ci în factori arbitrari.

Utilitatea unei asemenea descompuneri se vede considerând din nou
modelul de clasificare Na\"{i}ve Bayes.  Modelul este unul generativ,
întrucât odată antrenat, poate fi folosit atât pentru calculul $P(Y|X)$
cât și $P(X|Y)$ -- modelul capturează de fapt întreaga distribuție $P(X,Y)$.
De multe ori, aspectul generativ nu este necesar, și ar fi de preferat un
model care să abordeze doar distribuția discriminativă $P(Y|X)$, dar eventual
să permită învățarea mai eficientă, în schimb.

Obținem un astfel de model, regresia logistică, eliminând sensul săgeților
din graful Na\"{i}ve Bayes (figura \ref{fig:lr}).

Pentru a beneficia de faptul că modelele condiționale nu se interesează
de distribuția variabilelor observate, parametrizarea se face adesea
prin intermediul unei funcții de mapare a trăsăturilor $f(x, y)$:
\begin{equation}
P(y|x) = \frac{1}{Z} \exp\left(w^T f(x, y)\right)
\end{equation}
Clasificarea se face căutând atribuirea MAP, dar spre deosebire de modelul
bayesian naiv, regresia logistică nu poate genera și nici calcula
verosimilitatea datelor.
\section{Modele dinamice cu șabloane}
De multe ori, structura ce trebuie modelată conține multe elemente simetrice
și interacții identice dar între variabile diferite.  Rețelele cu parametri
astfel repetați sunt utile pentru că suferă mai puțin de fenomenul de supraînvățare,
și permit modelarea structurilor de dimensiuni variate, folosind un set de
parametri constant.

Un tip simplu de modele dinamice cu șabloane sunt modelele secvențiale sau
temporale.  Acestea țintesc să captureze evoluția unei sau mai multor variabile
în timp, de exemplu starea vremii (umiditate, presiune atmosferică, temperatură).
Pentru a permite modelarea unor secvențe de timp lungi fără ca parametrii, și
automat complexitatea sistemului să crească, se folosește {\bf presupunerea Markov}:
viitorul și trecutul sunt independente dat fiind prezentul.  Formal,

\begin{equation}
X^{(t + 1)} \bot X^{(t - 1)} | X^{(t)} \forall t = \overline{1,...,n}
\end{equation}

Presupunerea duce la un model grafic desfășurat ca în figura \ref{fig:hmm_unrolled}, ce poate fi
reprezentat ca un șablon precum în figura \ref{fig:hmm_tpl}.

Modelele Markov ascunse (Hidden Markov Models - HMM), în formularea cea mai simplă,
sunt astfel de modele în care la fiecare etapă de timp avem o variabilă latentă
(ascunsă), ce descrie starea internă sistemului, și o variabilă observabilă.  HMM
sunt modele generative: pot fi folosite atât pentru etichetare (dată fiind
o secvență de măsurători de umiditate, să se recreeze secvența de temperaturi)
cât și pentru generare (modelul poate propune o secvență plauzibilă de stări
ascunse, și pentru fiecare dintre ele, câte o observație).

Factorizarea distribuției reprezentate de un HMM este:
\begin{equation}
P(X_1, ..., X_n, Y_1, ... Y_n) = P(Y_1) \prod_{t=1}^T P(X_t|Y_t) \prod_{t=1}^{T-1}P(X_{t+1}|X_t)
\end{equation}

Pentru parametrizare convenabilă, se presupune probabilitățile de tranziție și
de observație nu depind de timp:
\begin{equation}P(X_{t+1}=a|X_{t}=b) = p(a, b) \forall t\end{equation}
\begin{equation}P(X_t=a|Y_t=y) = o(a, y) \forall t\end{equation}


Numele de modele Markov vine de la matematicianul Andrey Markov, care le-a
utilizat prima oară într-o formă simplă (fără variabile ascunse) pentru
studiul succesiunilor de caractere în text într-un roman de Pușkin.  Nu este
surprinzător că modelele de acest tip au găsit numeroase utilități în
procesarea limbajului natural.

Așa cum puteam transforma un model naiv Bayesian într-un model de regresie
logistică, îmbunătățind puterea de discriminare cu prețul eliminării aspectului
generativ, la fel putem defini o variantă discriminativă de tip log-linear a
modelelor Markov ascunse.  Aceste modele se numesc câmpuri aleatoare
condiționale de tip lanț liniar (linear chain conditional random fields).
Ca și în cazul regresiei logistice, se sare peste modelarea probabilității
variabilelor observate și se modeleaza direct probabilitatea condiționată:
\begin{equation}
P(Y|X; w) = \frac{1}{Z} \exp\left(\sum_{p\in\mathcal{P}} w^T f_p(x, y_p)\right)
\label{eq:vero}
\end{equation}
Funcțiile-trăsături $f_p$ capturează atât trăsăturile la nivelul vârfurilor
($p=1, ..., T$) cât și cele la nivelul muchiilor ($p=(1, 2), ..., (T-1, T)$).
În general, parametrizarea de mai sus permite extinderea la grafuri arbitrare,
prin definirea corespunzatoare a mulțimii $\mathcal{P}$.
\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.3\linewidth}
\centering
\input{img/hmm_tpl}
\caption{Șablonul modelului HMM}
\label{fig:hmm_tpl}
\end{subfigure}
\begin{subfigure}[b]{0.65\linewidth}
\input{img/hmm}
\caption{HMM instanțiat pentru o lungime concretă}
\label{fig:hmm_unrolled}
\end{subfigure}
\caption{Model Markov ascuns de ordin 1}
\label{fig:hmm}
\end{figure}
\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.5\linewidth}
\centering
\input{img/lccrf_tpl}
\caption{Șablonul corespunzător modelului CRF de tip lanț liniar.  Variabilele observate sunt gri pentru că nu sunt modelate probabilist.}
\label{fig:lccrf_tpl}
\end{subfigure}
\begin{subfigure}[b]{0.5\linewidth}
\input{img/lccrf}
\caption{Modelul CRF de tip lanț liniar, instanțiat pentru o lungime concretă.  Variabilele observate nu sunt modelate probabilist deci nu sunt reprezentate.}
\label{fig:lccrf}
\end{subfigure}
\caption{Câmpul aleator condiționat (CRF) de tip lanț liniar}
\label{fig:crfbig}
\end{figure}
\section{Inferență}
În general, inferența în modele grafice de probabilitate se referă la efectuarea
de interogări ale probabilităților condiționale, opțional în prezența unor
observații.  Pentru exemplificare, vom atribui o interpretare variabilelor
din modelul din figura \ref{fig:someconn}.
\begin{itemize}
\item $A$: inteligența unui student
\item $B$: nota studentului la examen
\item $C$: experiența profesională a studentului
\item $D$: decizia de acceptare la școala doctorală
\end{itemize}
Dintre inferențele interesante care se pot calcula, exemplificăm câteva:
\begin{itemize}
\item Studentul știe că a luat nota 8 și că are experiența medie, și vrea să
știe care e probabilitatea să fie acceptat: $$P(D=da|B=8, C=mediu)$$
\item Comisia de admitere trebuie să ia decizia de acceptare.  Ei nu pot
observa direct inteligența studentului, dar au acces la notă și la
experiența studentului.  Se caută valoarea $d$ care maximizează:
$$\operatorname{arg\,max}_d P(D=d|B=8, C=mediu)$$
Aceasta se poate calcula direct din parametrii modelului.
\item Ministerul Educației ar putea fi curios de probabilitatea ca un
student admis la școala doctorală să fie slab, ca o măsură a
slăbiciunii sistemului.
\end{itemize}
\begin{equation*}
\begin{aligned}
& P(A=\text{slab}|D=\text{da}) = \frac{P(D=\text{da}|A=\text{slab}) P(A=\text{slab})}{P(D=\text{da})} = \\
= & \frac{P(A=\text{slab})\sum_b P(B=b|A=\text{slab}) \sum_c P(C=c|A=\text{slab}) P(D=\text{da}|B=b, C=c)}
{\sum_a P(A=a)\sum_b P(B=b|A=a) \sum_c P(C=c|A=a) P(D=\text{da}|B=b, C=c)}
\end{aligned}
\end{equation*}    

În problemele de clasificare, tipul cel mai important de
inferență este așa-numita inferență MAP (maximum a posteriori):
găsirea unei atribuiri optime pentru variabilele de interes,
date fiind dovezile.  Exemplul al doilea de mai sus este
inferență de tip MAP.

Modelele de învățare structurală se pot descrie ca modele grafice în care
variabilele se împart în variabile observate (de intrare) $X$ și variabile
ascunse (țintă) $Y$.  Inferența MAP joacă rolul etapei de predicție, de
aplicare a modelului pentru a obține etichetele datelor de intrare.  Cu alte
cuvinte, $$h_{\text{MAP}}(x) = \operatorname{arg\,max}_y P(y|x; w)$$
Expresia de mai sus joacă rolul funcției predictor $h$ din ecuația \ref{eq:pred}. 

Chiar pentru un model simplu, unele inferențe aparent
simple pot necesita calcule complicate, cum reiese din exemplul
de mai sus.  În general, inferența într-un model oarecare este
o problemă tare NP, și pe cazul general chiar și inferența
aproximativă este tare NP. Pentru modele particulare, însă, chiar și inferența
exactă poate deveni tractabilă, mai ales dacă variabilele observate sunt mereu
aceleași.  Când graful suport conține bucle, inferența exactă este impractică,
fie și pentru grafuri de dimensiuni mici.

%\subsection{Inferența în modelul Bayesian naiv}
%În modelul Naive Bayes pentru
%clasificare, dacă presupunem că mereu se pot măsura toate
%variabilele $X_i$, inferența este rapidă.

\subsection{Inferența în modele secvențiale}
Pentru lanțuri de ordinul I, adică modele ce verifică presupunea
Markov, atribuirea MAP se poate calcula rapid dați fiind parametrii și
observațiile folosind algoritmul înainte-înapoi (forward-backward),
o formă particulară a unui algoritm mai general numit propagarea
încrederii (belief propagation).

Densitatea de probabilitate reprezentată de un model Markov
ascuns poate fi scrisă sub forma unui produs de factori:

$$P(y, x) = \prod_t \Psi_t(y_t, y_{t-1}, x_t)$$
unde factorii au forma:
$$\Psi_t(j, i, x) = P(Y_t=j|Y_{t-1}=i)P(X_t=x|Y_t=j)$$

Algoritmul înainte-înapoi se bazează calculul unor variabile
intermediare, variabilele {\em înainte} \alpha și variabilele
{\em înapoi} \beta, definite astfel:
$$\alpha_t(j) = P(X_1=x_1, ... X_t = x_t, y_t=j)$$
$$\beta_t(i) = P(X_{t+1}=x_{t+1}, ..., X_T=x_T|Y_t=i)$$
și calculate recursiv astfel:
$$\alpha_1(j) = \Psi_1(j, y_0, x_1)$$
$$\alpha_t(j) = \sum_i \Psi_t(j, i, x_t)\alpha_{t-1}(i)$$
$$\beta_T(i) = 1$$
$$\beta_t(i) = \sum_j \Psi_{t+1}(j, i, x_{t+1})\beta_{t+1}(j)$$

Apoi, probabilitatea căutată, $P(Y_{t-1}, Y_t|x)$ se poate
obține calculând:
$$P(Y_{t-1}, Y_t, x) = \alpha_{t-1}(y_{t-1})\Psi_t(y_t, y_{t-1}, x_t)\beta_t(y_t)$$
și normalizând după $y_t, y_{t+1}$.

În CRF-uri de tip lanț, algoritmul rămâne identic, dar factorii
sunt definiți:
$$\Psi_t(y_t, y_{t-1}, x_t) = \exp\left(\sum_k w_k f_k(y_t, y_{t-1}, x_t)\right)$$

Inferența MAP se poate realiza la fel de ușor, înlocuind sumele cu
luarea maximului, și produsele cu sume.
În practică, pentru stabilitate numerică, fie se lucrează în domeniul
logaritmic, fie se aplică scalarea variabilelor {\em înainte-înapoi}.
Implementarea din pachetul {\em CRFsuite}~\citep{crfsuite} folosit
în lucrarea de față folosește scalarea descrisă în~\citep{crfinference}.
\subsection{Algoritmi generali pentru inferență}
În modele grafice arbitrare, cu bucle, inferența trebuie realizată prin
metode aproximative.  Biblioteca {\em pystruct}~\citep{pystruct}, în construcție, care
permite antrenarea de modele grafice arbitrare în limbajul Python, pune
la dispoziție interfețe pentru câteva pachete renumite pentru inferență
arbitrară, pe care le vom descrie în continuare.
\subsubsection{libDAI}
Pachetul {\em libDAI}~\citep{libdai}, unde {\em DAI} vine de la {\em discrete approximate
inference} (inferență aproximativă discretă), pune la dispoziție metode
standard atât pentru inferență exactă (enumerare prin forță brută,
arbore de joncțiune) cât și pentru inferență aproximativă (diverse
variațiuni ale algoritmului {\em belief propagation}, enumerate, cu
referințe, pe pagina bibliotecii\footnote{\url{http://cs.ru.nl/~jorism/libDAI/}}.)
\subsubsection{Relaxare în programare liniară}
Inferența MAP se poate scrie ca o problemă de programare liniară: 
\begin{equation*}
\begin{aligned}
\text{max\,} & \theta^T \mu \\
\text{cu\,constrângerea\,} & \mu \in \operatorname{MARG}(\mathcal{G})
\end{aligned}
\end{equation*}
În ecuația de mai sus, $\operatorname{MARG}(\mathcal{G})$ este politopul
marginal al modelului $\mathcal{G}$, ale cărui vârfuri sunt vectori $y$
de atribuire ai variabilelor-țintă $Y$, iar punctele interioare corespund
vectorilor de probabilitate marginală realizabili de o anumită valoare a
parametrilor.  Parametrizarea $\theta$ nu este decât o transformare
liniară între spațiul trăsăturilor și spațiul obiectelor de intrare.
Conform~\citep[p.\~54]{martens}, deși în condițiile teoremei Minkowski-Weyl, politopul
marginal se poate reprezenta ca un număr finit de inegalități liniare, este
conjecturat că numărul de astfel de inegalități crește superpolinomial cu
dimensiunea grafului.  Dacă nu ar fi așa, întrucât problema tăieturii maxime
se poate reduce la o problemă MAP, s-ar demonstra că $P=NP$.
În ciuda impracticalității acestei formulări, o utilă formulare a
inferenței MAP aproximative este dată de~\citet{schl}:
\begin{equation*}
\begin{aligned}
\text{max} & \theta^T \mu \\
\text{cu constrângerea\,} & \mu \in \operatorname{LOCAL}(\mathcal{G})
\end{aligned}
\end{equation*}
De această dată, politopul $\operatorname{LOCAL}(\mathcal{G})\supseteq\operatorname{MARG}(\mathcal{G})$ este o aproximare în termeni de consistență locală. Deși
se poate demonstra egalitatea pentru grafuri aciclice, în general incluziunea
este strictă.  Rezolvarea acestei aproximări se poate face prin metode standard
de programare liniară.  {\em pystruct}~\citep{pystruct} propune inferență prin programare liniară
folosind pachetul {\em CVXOPT}~\citep{cvxopt}, ce folosește la rândul lui
{\em glpk}~\citep{glpk}.
\subsubsection{AD$^3$}
O dezvoltare recentă în domeniul inferenței în modele grafice este metoda
descompunerii duale, cunoscută și sub numele de relaxare Lagrangiană.
Metoda este clasică în domeniul optimizării, dar a fost propusă recent
ca metodă de inferență.  Această metodă propune o soluției a relaxării LP
definite deasupra prin reformulare și dualizare, ce permite apoi descompunerea
inferenței într-o problemă principală {\em master} ce constă în minimizarea
unei sume de probleme de optimizare secundare ({\em slave}), locale.  Puterea
metodei este dată de localitatea problemelor {\em slave}: în modele grafice
în care factorii sunt de natură foarte diferită între ei, precum când se dorește
introducerea de constrângeri deterministe, unele probleme {\em slave} se pot
rezolva foarte ușor profitând de aceste proprietăți.

Una din cele mai noi forme de descompunere duală pentru inferență este dată
de algoritmul $AD^3$~\citep{martens}, a cărui implementare este pusă la dispoziție
de către autor\footnote{\url{https://github.com/andre-martins/AD3}}.
\subsubsection{QPBO}
Metoda QPBO ({\em quadratic pseudo-boolean optimization}) folosește principiul
{\em roof duality}, bazat pe o altă relaxare a formulării ca programare
liniară.  Conform~\citep{kolm}, se relaxează atribuirile booleene $y_i \in \{0, 1\}$
la constrângeri fracționare $y_i \in [0, 1]$.  Se poate arăta că problema de
programare liniară corespunzătoare are o soluție cu valori în $\{0, 1, 0.5\}$
din care se poate extrage o atribuire parțială ignorând valorile $0.5$.
Rezolvarea acestei probleme se poate face prin tăieturi de grafuri.
\section{Învățarea parametrilor}
Antrenarea unui model de etichetare secvențială este procesul prin care parametrii
modelului sunt estimați pe baza datelor observate.  Pentru modelele generative
precum HMM, antrenarea se face cu o variantă a algoritmului
{\em expectation-maximization} (EM) cunoscută sub numele de algoritmul Baum-Welch.
Prezentăm ideea algoritmului pe scurt, întrucât în lucrarea de față folosim
numai metode discriminative de antrenare.
Algoritmul alternează pasul $E$, ce constă în inferența marginală $P(Y|X=x; w)$
și pasul $M$, ce constă în actualizarea parametrilor pentru maximizarea
log-verosimilității așteptate
$$w' \leftarrow \operatorname{arg\,max_w} \mathbf{E}_{Y|X=x, w}[\ell(w; x, Y)]$$
Se observă nevoia efectuării inferenței la fiecare pas.  Întrucât inferența
este necesară pentru calcularea log-verosimilității sau pentru calcularea
etichetării MAP, vom vedea că toate metodele de antrenare discriminative
suferă de aceeași nevoie de a efectua inferența frecvent.

În prezentarea metodelor de antrenare discriminative răspândite nu vom urma
ordinea istorică ci o ordine a complexității și generalității metodelor.

\subsection{Perceptronul structurat}
Unul din cele mai vechi metode de clasificare nestructurată este perceptronul
\citep{blatt}.  În cazul nestructurat de clasificare binară, perceptronul
învață o regulă de decizie liniară dată de semnul corelației cu vectorul de
parametri:
$$h(x) = \operatorname{sgn}(w^Tx)$$
Pentru ușurința explicatiei presupunem că cele două clase sunt $\mathcal{Y}=\{-1, 1\}$. Atunci, algoritmul perceptronului constă în alegerea unui punct de antrenare
$(x_i, y_i)$ și actualizarea parametrilor prin regula:
\begin{equation}
w' = w + 0.5 (y_i - h(x_i))y_ix_i
\end{equation}
Dacă punctul ales este clasificat corect, termenul $y_i - h(x_i)$
se anulează și parametrii nu sunt actualizați.  Dacă punctul este misclasat,
vectorul de parametri este ajustat în direcția corelației căutate.

O variantă interesantă este perceptronul în medie.  Acesta, la sfârșitul antrenării,
în loc să folosească ultima valoare a vectorului de parametri $w^{(T)}$,
folosește media aritmetică $\frac{1}{T}\sum_{t=1}^{T} w^{(t)}$.  În
practică, metoda mediei dă naștere unui clasificator mai robust reducând efectele
date de ordinea alegerii exemplelor.

Adaptarea la predicția structurată se datorează lui \citet{collins}, ce a aplicat
metoda la problema etichetării părților de vorbire.  Perceptronul structurat
are regula de decizie:
\begin{equation}
\hat{y} = h(x) = \underset{y \in \mathcal{Y}}{\operatorname{arg\,max}} w^T f(x, y)
\end{equation}
și regula de actualizare:
\begin{equation}
w' \leftarrow w + f(x_i, y_i) - f(x_i, \hat{y})
\end{equation}

În final se poate folosi fie ultima valoare a parametrilor, fie media.

Perceptronul în medie este implementat în pachetul {\em
CRFsuite} ~\citep{crfsuite} pentru lanțuri liniare.  Biblioteca {\em pystruct}
~\citep{pystruct}
implementează perceptronul atât cu și fără luarea mediei, prin contribuția
proprie.
\subsection{Algoritmi pasiv-agresivi}
O clasă de extensii înrudite la algoritmul perceptronului au fost propuse
cu succes, întrecând pentru multe aplicații performanța acestui algoritm simplu.
Discutăm în cele ce urmează algoritmul denumit PA-I, îndrumând cititorul
către~\citep{pa} pentru detalii despre alte variante.

Ca și algoritmul perceptronului, la fiecare pas se analizează un
exemplu $(x_i, y_i)$.  Actualizarea parametrilor se face minimizând:
\begin{equation}
\begin{aligned}
\operatorname{min}_w & \frac{\lambda}{2}||w - w'||^2 + \xi \\
\operatorname{cu constrângerile} & \xi > 0 \\
& wf(x_i, y_i) \geq wf(x_y, \hat{y}) + \rho(\hat{y}, y_i) + \xi
\end{aligned}
\label{eq:pa}
\end{equation}
unde $\hat{y}$ este argumentul ce maximizează așa-numita inferență
îmbogățită prin cost:
\begin{equation}
\operatorname{arg\,max}_{y'} w^T f(x_i, y') + \rho(y', y_i)
\end{equation}

Altfel spus, algoritmul PA-I este un perceptron cu pas de mărime
variabilă, interpolând între a păstra vectorul $w$ pe loc (prin
obiectivul problemei de optimizare) și a clasifica cu o margine
destul de mare exemplul de antrenare analizat.  Dacă rezultatul inferenței
$\hat{y}$ este corect, atunci algoritmul lasă vectorul $w$ neschimbat,
ca și perceptronul.  Altfel, are loc o actualizare de forma:

\begin{equation}
w' \leftarrow w + \eta_t(f(x_i, y_i) - f(x_i, \hat{y}))
\end{equation}

unde $\eta_t$ este mărimea pasului și se poate calcula, în formă
închisă, pentru a optimiza problema \ref{eq:pa}:

\begin{equation}
\begin{aligned}
\ell = w^T f(x_i, \hat{y}) - w^T f(x_i, y_i) + \rho(\hat{y}, y_i) \\
\eta = \min\left\{ \lambda^{-1}, \frac{\ell}{||f(x_i, y_i) - f(x_i, \hat{y})||^2}\right\}
\end{aligned}
\end{equation}
De aici numele metodei:  vectorul de ponderi rămâne {\em pasiv} dacă
clasificarea este bună, și este ajustat {\em agresiv}, cât să clasifice
cu o margine destul de mare exemplul, dacă are loc misclasarea.

Învățarea pasiv-agresivă de mai multe tipuri este implementată în pachetul {\em
CRFsuite} \citep{crfsuite} pentru lanțuri liniare.

\subsection{Regularizarea adaptivă a ponderilor (AROW)}
Algoritmul AROW \citep{arow} este strâns legat de algoritmii PA, dar în locul
actualizării unui vector de parametri, AROW păstrează o distribuție
gaussiană parametrizată prin $\mu, \Sigma$ a vectorului de ponderi.  Pentru
clasificare, respectiv inferență, se folosește media învățată.

Optimizarea efectuată la fiecare rundă minimizează ecuatia:
\begin{equation}
C(\mu, \Sigma) = \operatorname{D}_\text{KL}(\mathcal{N}(\mu', \Sigma') || \mathcal{N})) + \lambda_1 \rho(y_i, \mu x_i) + \lambda_2 x_i^{T} \Sigma x_i
\end{equation}

Parametrii $\lambda_1$ și $\lambda_2$ de regulă se pun la un loc sub forma:
$$\lambda_1 = \lambda_2 = \frac{1}{2r}, r > 0$$

Ecuațiile de actualizare pentru $\mu$ și $\sigma$ sunt:

\begin{equation}
\begin{aligned}
\mu' & = & \mu + \alpha y_ix_i \\
\Sigma' & = & \Sigma - \beta' \Sigma x_i x_i^T \Sigma \\
\alpha' & = & \operatorname{max}(0, 1 - y_ix_i^T\mu)\beta' \\
\beta' & = & \frac{1}{x_i^T \Sigma x_i + r}
\end{aligned}
\end{equation}

Învățarea AROW este implementată în pachetul {\em CRFsuite} \citep{crfsuite}
pentru lanțuri liniare.

\subsection{Mașini cu vectori suport structurate}
Unul dintre algoritmii de învățare supervizată cei mai răspândiți,
datorită rezultatelor bune obținute la multe probleme diferite cu
ajustări relativ simple, este mașina cu vectori suport (SVM).

În cazul nestructurat, SVM-urile liniare~\citep{vapnik} sunt modele ce maximizează
marginea predicției.  La baza lor stă funcția de pierdere balama
({\em hinge}), denumită astfel datorită formei graficului său:
\begin{equation}
L_{\text{hinge}}(w; x, y)) = \max(0, 1 - y \cdot h(w; x))
\end{equation}
Parametrii sunt obținuți minimizând funcția-obiectiv
\begin{equation}
E(w, b) = \sum_{i=1}^{M} L_{\text{hinge}}(y_i, wx_i + b) +
          \frac{\alpha}{2}\sum_{i=1}^{d} w_i^2
\end{equation}

Pentru SVM-ul structurat, funcția de pierdere corespunzătoare, numită {\em structured hinge loss}, se scrie:
\begin{equation}
L_{\text{SSVM}}(w; x, y) = -w^T f(x, y) + \max_{y'} \left(w^T f(x, y') + \rho(y', y)\right)
\end{equation}

Problema de optimizare corespunzătoare este \citep{taskar}:
\begin{equation}
\begin{aligned}
\operatorname{min} & \frac{1}{2} ||w||^2 + C\sum_x \xi_x \\
\operatorname{cu\,constrângerea\,} & w^T f(x_i, y_i) \geq w^T f(x_i, \hat{y}) + \rho(\hat{y}, y_i) \xi_x \forall i 
\end{aligned}
\end{equation}

Se observă că algoritmul PA descris mai sus poate fi privit ca o variantă
online a algoritmului SVM.  Rezolvarea problemei de optimizare a mașinii
cu vectori suport structurale nu se poate rezolva prin coborâre stochastică
pe gradient, dar se poate folosi metoda coborârii stochastice pe subgradient.
În lucrarea de față, însă, folosim algoritmii de optimizare convexă
standard implementați în \citep{cvxopt} în mod {\em batch}, pentru
corectitudine sporită.  Ambele variante sunt implementate în pachetul
{\em pystruct}~\citep{pystruct} pentru grafuri arbitrare.

\subsection{Maximizarea entropiei (algoritmul CRF)}
Lăsăm la urmă varianta originală de antrenare propusă de~\citet{crf},
unde parametrii sunt aleși să maximizeze verosimilitatea datelor (ecuația \ref{eq:vero}).
Metoda se mai numește și {\em etichetare prin maximizarea entropiei}, deoarece
minimizarea funcției de cost asociată este duala unei probleme de alegere a
unei densități de repartiție cu entropie cât mai mare și momentele aliniate cu
cea estimată.  Terminologia și formularea nu este mai mult decât o aplicare la
modelul secvențial a analogiei dintre Na\"ive Bayes și regresia logistică (și
ea numită, în literatură, clasificare {\em maxent}).

Regresia logistică minimizează log-verosimilitatea negativă, ce dă naștere 
funcției de pierdere:
\begin{equation}
L_{\text{LR}}(w; x, y) = -w^T f(x, y) + \log \sum_{y'} \exp (w^T f(x, y'))
\end{equation}

În cazul modelelor structurate ce maximizează entropia (CRF), funcția de pierdere
are exact aceeași formă, dar prin structura funcției de trăsături, se poate
descompune:
\begin{equation}
L_{\text{CRF}}(w; x, y) = \log \sum_{y'} \exp \left(
\sum_{p\in\mathcal{P}}w^T(f_p(x, y'_p) - f_p(x, y_p))
\right)
\end{equation}
Întrucât funcția este derivabilă, minimizarea se poate face și cu algoritmul
de coborâre stochastică pe gradient.  Atât aceasta variantă de implementare,
cât și cea {\em batch}, folosind algoritmul L-BFGS, sunt disponibile
în pachetul {\em CRFsuite}~\citep{crfsuite} pentru lanturi liniare.
