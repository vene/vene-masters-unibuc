\chapter{Introducere}

Concomitent cu creșterea puterii de calcul disponibile, atât pentru
universități, în industrie dar și în calculatoarele personale, cu avansul
metodelor de stocare și de achiziție a datelor, cercetarea în domeniul
învățării automate este în plină explozie.  Probleme ce au fost abandonate în
ciuda investițiilor enorme în anii 1970 sunt astăzi implementate în produse
disponibile clienților.  Poate cele mai vizibile sunt sarcinile ce au de a face
cu limbajul natural: traducerea automată este astăzi un produs la îndemâna
consumatorilor.

Progresul a venit în consecința schimbării de paradigmă în domeniul
inteligenței artificiale: trecerea de la modele bazate pe reprezentarea
cunoștințelor, cu cost ridicat prin necestitatea implicării experților,
la metode statistice, alimentate de date \citep{unreasonable}.  Acestea
sunt și ele costisitoare, dar în termeni de putere de calcul și disponibilitate
a datelor -- cost care scade mult mai rapid decât primul.

În domeniul procesării limbajului natural (abreviat în continuare NLP,
pentru consistență cu denumirea universală {\em natural language processing}),
la nivelul sarcinilor de nivel jos dominate de ambiguitate, metodele statistice
au întrecut semnificativ performanța celor bazate pe reguli.  În consecință,
activitatea de cercetare s-a concentrat din ce în ce mai mult pe dezvoltarea de
unelte și resurse pentru modele statistice.  Progresul în domeniu va fi
descris în secțiunea \ref{sec:nlp}.

În prezent, în urma dezvoltării și modularizării tehnicilor de învățare
automată, cercetarea atacă în paralel fețele multiple, cuplate, ale
domeniului.  În primul rând, proiectarea modelelor prezintă un
compromis între modele simple, ușor de folosit și antrenat, dar fără
suficientă putere de a captura variabilitatea domeniului modelat, și
modele complexe, puternice, care pentru a fi folosite eficient necesită
metode de antrenare robuste și colecții mari de date.  În NLP, unele
sarcini sunt suficient de bine capturate de modele simple, iar altele
necesită flexibilitate mai mare.

În al doilea rând, predicția, sau inferența, are un impact major asupra
practicalității utilizării modelelor în aplicații.  Etapa predicției înseamnă
utilizarea modelului pentru sarcina pentru care a fost conceput, de exemplu
luarea unei decizii, etichetarea unui obiect sau calcularea probabilității
întâlnirii unei situații\footnote{Cu puțin efort, putem formula și alte astfel
de contexte de învățare: gruparea obiectelor, ordonarea lor după vreun criteriu
ascuns, etc.  La un nivel potrivit de abstractizare, însă, ele se pot rescrie
toate unul în funcție de altul.}.  Dacă pentru unele modele interogarea e
trivială, în multe cazuri este costisitoare și se investește mult efort în
îmbunătățirea metodelor.

În al treilea rând, antrenarea, numită și învățare sau optimizare, implică
adaptarea parametrilor modelului la datele disponibile.  Chiar și pentru
modele relativ simple precum regresia logistică, găsirea parametrilor
optimi global nu este tractabilă.  Prin urmare, există un efort intens de
cercetare în domeniul optimizării pentru învățarea automată.  Nu mai este
un mister astăzi că algoritmii complicați de optimizare numerică din domeniul
cercetărilor operaționale, precum metodele quasi-Newton, sunt adesea
întrecute de simpla coborâre stocastică pe gradient (SGD), la criteriile
relevante pentru învățarea automată.  Journal of Machine Learning Research
a dedicat un număr special interacției dintre cele două domenii \citep{opti},
iar folclorul în rândul celor care aplică învățarea automată în industrie
include idei precum briciul lui Occam, sau \enquote{învățarea e mai ușoară
decât optimizarea}\footnote{\enquote{Learning is easier than optimiztion},
atribuită lui Leon Bottou.
\url{http://www.machinedlearnings.com/2013/04/learning-is-easier-than-optimization.html}}.
Metode puternic aproximative, precum trucul {\em hashing} \citep{hash}, dau
rezultate surprinzătoare.  Acesta, deși intuitiv pare o aproximare care crește
viteza prelucrării cu prețul calității reprezentării, în practică în multe
cazuri obține rezultate mai bune decât metode exacte, tocmai pentru că
informația pierdută este adesea doar zgomot.  Un comportament similar are
trucul folosirii distanței {\em Rank} \citep{dinurd} între clasamentele după
frecvență a cuvintelor-cheie, în locul frecvențelor brute, în sarcini precum
identificarea de autor \citep{dinuworkshop}. 

În lucrarea de față, lupa este pusă pe modelele adecvate învățării
structurate secvențiale.  Vom trece în revistă metodele de inferență
și de învățare necesare, și disponibile sub formă de biblioteci.  Vom
discuta sarcini din NLP la nivel de cuvânt, silabă și literă.


\section{Învățăre automată, învățare structurată}
Secțiunea curentă este dedicată fixării formalismului, notației dar și
a intuițiilor conceptelor fundamentale de învățare automată și de
învățare structurală.

\subsection{Sarcini de învățare automată}

O formulare vagă a experienței de învățare, raportată la sisteme informatice,
este dată de \citet{mitchell}:

\begin{quote}
Un program informatic se spune că învață din experiența \textbf{E} față de o
clasă de sarcini \textbf{T} și o măsură a performanței \textbf{P}, dacă
performanța sa la sarcini din \textbf{T}, măsurată cu \textbf{P}, crește prin
experiența \textbf{E}.
\end{quote}

Un mod practic de a privi problemele de învățare, cât mai general cu putință,
este ca predicție.  În mod abstract, sarcinile luate în considerare se pot
rezuma la asocierea unor ieșiri $y$ corespunzătoare unor date de intrare $x$.
În cazul general, nu facem nicio presupunere asupra spațiului sau formei
acestor obiecte, încadrându-le pur și simplu în spații arbitrare $\mathcal{X}$,
respectiv $\mathcal{Y}$.  Astfel, problema revine la găsirea unui predictor
\begin{equation}
h : \mathcal{X} \rightarrow \mathcal{Y}
\label{eq:pred}
\end{equation}
care să rezolve \enquote{bine} problema. Ce înseamnă cuvântul \enquote{bine} va
fi abordat în secțiunea despre evaluare.

Învățarea structurată tratează cazul când spațiile $\mathcal{X}$ și
$\mathcal{Y}$ sunt încărcate cu o structură mai bogată decât simple colecții de
valori.  

Este important de remarcat că învățarea structurată constă într-o clasă
de metode și abordări, un model ce trebuie adaptat în fiecare caz, și nu
un nod terminal în structura arborescentă a teoriei și practicii învățării
automate.

În literatură uneori obiectele $x$ sunt definite strict ca vectori, pentru că
algoritmii de învățare automată sunt bazați aproape exclusiv pe operații
numerice.  În lucrarea de față vom păstra spațiul $\mathcal{X}$ arbitrar.
Reprezentarea numerică revine în sarcina funcției de extragere de trăsături:
\begin{equation}
f : \mathcal{X} \times \mathcal{Y} \rightarrow \mathbf{R}^d
\end{equation}

Tipurile de învățare cel mai frecvent întâlnite se potrivesc natural formulării,
după cum urmează:

\subsubsection{Clasificare}
Sarcina de clasificare înseamnă încadrarea fiecărui exemplu în exact o clasă
dintr-o listă stabilită, nesupusă schimbării. În acest caz, $$\mathcal{Y} = \{
c_1, c_2, ..., c_k \}$$.  De exemplu, identificarea cifrelor scrise de mână
este o problemă de clasificare: $x$ este imaginea unei cifre, $\mathcal{Y} =
\{0, 1, 2, ..., 9\}$ iar $h$ are sarcina de a atribui unei imagini, valoarea
cifrei.  Atragem atenția că $\mathcal{Y}$ nu are nici un fel de structură
matematică, deși la prima vedere ar permite multe.  Nu avem nici un fel de
relație între obiectele lui $\mathcal{Y}$ și nicio operație între ele.  Clasele
sunt doar niște simboluri arbitrare, denumite tradițional {\em etichete}, și
le-am putea înlocui facil cu $\mathcal{Y} = \{zero, unu, doi...\}$ de exemplu.

Un caz particular este clasificara multi-etichetă, care se poate reduce la
clasificarea obișnuită, dar cu $y \in Y' = \mathcal{P}(Y)$.  Un exemplu natural
este clasificarea articolelor de știri pe domenii.  Putem considera domenii
precum {\em istorie} și {\em sport}, dar nu este greu de imaginat cum unele
articole pot fi în egală măsură despre două sau mai multe astfel de domenii, de
exemplu un articol în onoarea unui personaj-simbol pentru un club sportiv.
Nici în acest caz nu introducem, de exemplu, structura de algebră Boole a
mulțimii părților.

\subsubsection{Regresie}
Regresia constă în atribuirea unei valori numerice obiectului dat. În acest
caz, $\mathcal{Y} = [a, b]$ sau chiar $\mathbf{R}$, în limitele capacității de
reprezentare a arhitecturii de calcul folosite.  De exemplu, predicția prețului
de vânzare a unei locuințe.  În acest tip de probleme, spațiul $\mathcal{Y}$ are
o noțiune de distanță între elemente, de regulă de modulul diferenței
$\ell_1(y_1, y_2) = ||y_1 - y_2||$ sau de pătratul ei $\ell_2 = (y_1 - y_2)^2$.
Cazuri particulare importante sunt estimarea de densitate, unde variabila-țintă
$y$ reprezintă probabilitatea evenimentului $x$, sau regresia izotonică.

\subsubsection{Detectarea de anomalii / valori aberante}
O problemă strâns legată de estimarea de densitate, și formulată foarte aproape
de o problemă de clasificare, este detectarea de anomalii: pentru o intrare $x$,
să se determine dacă (sau cât de sigur putem afirma că) $x$ e o valoare
aberantă pentru domeniul în care aparține.  Problema merită menționată separat
pentru este mai generală decât estimarea de densitate, pentru că nu pune
constrângerea ca $h$ să reprezinte o funcție de densitate de repartiție, dar
este mai particulară decât clasificarea, întrucât nu există \enquote{contraexemple}.

\subsubsection{Clasare}
Clasarea sau {\em ranking} este problema punerii în ordine după un anumit
criteriu a unei mulțimi de obiecte $x = \{x^1, x^2, ... x^p\}$. Problema se
aseamănă cu o regresie urmată de o sortare, dar prin formularea ei directă
se evită erorile pasului de regresie, întrucât valorile exacte nu sunt
necesare, ci doar ordinea lor.  Un exemplu des întâlnit zi cu zi este ordonarea
după relevanță a rezultatelor unui motor de căutare.

\subsubsection{Regresie ordinală}
Strâns legată este regresia ordinală, în esență o problemă de clasificare cu
o relație de ordine introdusă pe mulțimea claselor, $c_1 < c_2 < ... < c_n$.
De exemplu, predicția secolului în care a fost scrisă o operă, sau predicția
succesului în vânzări a unui film ce urmează să apară. 

\subsubsection{Etichetarea}
Etichetarea, în particular cea secvențială, face obiectul principal al lucrării
de față.  Problema este similară unei probleme multiple de clasificare, unde
obiectul $x$ constă din mai multe subobiecte $x^1, x^2, ..., x^p$ și fiecare subobiect
trebuie să primească o etichetă $y^1, y^2, ..., y^p$.  Problema nu este redusă
la o simplă aplicare multiplă a clasificării pentru că între subobiecte există
structuri motivate spațial, temporal sau altfel, fapt ce cauzează interdependența
în probabilitate a variabilelor corespunzătoare etichetelor.

Un exemplu celebru este etichetarea părților de vorbire ale cuvintelor dintr-un
text.  Structura de lanț a cuvintelor în propozitie este atât temporală, cât
și constrânsă lingvistic.  Partea de vorbire a unui cuvânt este strâns legată
de cea a cuvintelor dinainte și de după, de exemplu un articol hotărât va
precede aproape sigur un substantiv sau un adjectiv, dar rareori sau
niciodată\footnote{Din punctul de vedere al modelării probabiliste, \enquote{niciodată}
este un cuvânt periculos.  Scorurile nule de probabilitate se propagă pentru că
probabilitatea a unui eveniment compus este produsul probabilitătilor
evenimentelor individuale, iar orice număr înmulțit cu $0$ dă tot $0$.
Pe de altă parte, ponderile nule în vectorii de parametri sunt de dorit. Modelele
cu mulți parametri nuli sunt numite modele rare ({\em sparse}) și au numeroase
calităti.} un adverb sau o prepoziție.

\subsubsection{Învățarea de arbori}
Aproape de vârful complexității structurilor pe care se face învățare
structurată în prezent stau arborii, adică obiectele din $\mathcal{Y}$ să fie
arbori cu anumite proprietăți fixate.  Parsarea dependențelor sintactice în
limbajul natural este un exemplu în care inovațiile în învățarea structurală au
permis îmbunătățiri semnificative.


\subsection{Nevoia de învățare structurată}
O observație care se conturează în urma discuției de mai sus este că modelele
structurale nu sunt teoretic necesare.  Atâta timp cât spațiul $\mathcal{Y}$
poate fi enumerat, orice problemă structurală se poate reduce la un model
simplu de clasificare, fără a pierde din puterea de modelare.  Astfel, 
pentru etichetarea părților de vorbire, ne putem imagina un set de clase
$\mathcal{Y}$ ce constă din toate secvențele posibile de părți de vorbire,
pentru orice lungime de text întâlnit în domeniul $\mathcal{X}$ pe care se
lucrează.  Este evident că orice clasificator structurat $h$ se poate transforma
într-unul nestructurat care lucrează pe aceste spații.

În practică însă, astfel de abordări nu sunt fezabile, întrucât antrenarea unui
astfel de model ar fi imposibilă.  Legea lui Zipf atrage atenția asupra
probabilității descrescătoare a secvențelor lungi de cuvinte.  Un corolar
direct este descreșterea similară a probabilitătilor secvențelor lungi de părți
de vorbire, ceea ce duce la o șansă aproape de zero de a găsi mai mult de un
exemplu real pentru orice secvență de părti de vorbire dată.

Problema se accentuează și mai tare dacă ne imaginăm modele structurate mai
complexe.  Problema traducerii automate, de exemplu, constă esențial în
găsirea unei funcții $h$ care să ducă texte într-o limbă în texte
în altă limbă.  Chiar dacă am putea enumera ușor textele posibile în limba
țintă, este vizibil că nu am putea găsi decât cel mult un exemplu în orice
colecție de texte am încerca.  Prin factorizarea modelului la nivel de
paragrafe, apoi propoziții, apoi grupe de cuvinte, traducerea automată
statistică a putut deveni o realitate curentă.

La polul opus stă folosirea unor simplificări nestructurate, de regulă
urmată de metode ne-statistice pentru asigurarea consistenței.  Astfel
de modele sunt mai puțin expresive decât modelul structurat corespunzător,
neputând modela multe dependențe probabiliste, dar, întrucât sunt mult mai
simple, pot fi manevrate cu mai mare ușurință și rapiditate.  Pentru anumite
aplicații, astfel de abordări pot fi suficiente, dar în cele mai multe cazuri,
zgomotul introdus este prea mare. 

\subsection{Gradul de supervizare}
Învățarea automată se face din exemple, prin metode ce permit găsirea de
șabloane repetitive în date.  Supervizarea ține de forma datelor folosite.

\subsubsection{Învățare supervizată}
Când datele de antrenare disponibile sunt îmbogățite cu etichetarea ce trebuie
învățată, cadrul este cel al învățării supervizate.  Mulțimea de antrenare
are forma:
$$ \mathcal{T} = \{(x, y) : x \in \mathcal{X}, y \in \mathcal{Y}\} $$
Dificultatea este găsirea unui model care să generalizeze eficient la date
noi.  Învățarea supervizată este cazul cel mai bine studiat, și cel tratat
și în lucrarea de față.
\subsubsection{Învățare nesupervizată}
De multe ori nu se pot obține valorile luate de variabila-țintă $\mathbf{Y}$,
dar se poate specula cu privire la forma acesteia (de exemplu, numărul de
valori posibile pe care le poate lua).  Învățarea nesupervizată încearcă
să modeleze pe $\mathbf Y$ folosind doar exemple de date de intrare:
$$\mathcal{T} = \{x | x \in \mathcal{X}\}$$
Pentru a reuși sarcina, algoritmii de învățare nesupervizată se folosesc
de diverse similarități între elementele lui $\mathcal{X}$ pentru a găsi
șabloane și grupe de obiecte cu proprietăți comune.  Legătura dintre
relațiile găsite și variabila $\mathbf{Y}$ revine de regulă în sarcina
cercetătorului.
Uneori, chiar dacă etichetele $y$ s-ar putea obține, se preferă utilizarea
metodelor nesupervizate în loc, sau înainte de o metodă supervizată.  Astfel
se pot găsi structuri latente dincolo de puterea de modelare a unui algoritm
supervizat simplu, dar cu avantaje computaționale mari față de un algoritm
supervizat complex, destul de puternic pentru a modela el însuși asemenea
stări latente.  Această abordare este cunoscută sub numele de {\em învățare
nesupervizată de trăsături}.

\subsubsection{Învățare semisupervizată}
Unele metode ce îmbină elemente de ambele tipuri, pot îmbunătăți performanța
unui clasificator supervizat prin punerea la dispoziție a unei cantităti mai
mari de date neetichetate, dar ușor de obținut.  Acest tip de învățare se
numește semisupervizată și este utilă în multe scenarii în care datele
etichetate sunt greu de obținut, dar colectarea a cantități uriașe de date
neetichetate este la îndemână.
\section{Evaluare}
Evaluarea modelelor de învățare automată se face de obicei folosind
seturi de date ce pun la dispoziție o etichetare de referință, dar uneori,
modelele nesupervizate se pot evalua manual, când asemenea resurse nu există.

\subsection{Metrici de clasificare}
Măsurile de evaluare folosite în lucrarea de față sunt:
\begin{itemize}
\item {\bf Acuratețea}, abreviată în tabele {\em acc.} după eng. {\em accuracy},
reprezintă rata de succes a clasificatorului, și se poate defini indiferent
de numărul de clase și de structura spațiului de ieșire.  Dacă obiectele-țintă
din spațiul $\mathcal{Y}$ se pot descompune în instanțe atomice $y_p$,
atunci acuratețea este dată de $$\mathbf{E}_{i,p}\left[\mathbf{I}[y^i_p = \hat{y}^i_p]\right]$$
unde $\hat{y}$ este clasificarea produsă de sistem.
\item {\bf Precizia}, notată în tabele cu $P$, este o măsură la nivel de clasă,
de regulă raportată pentru clasa pozitivă, cea de interes.  Precizia este dată de
$$P = \frac{tp}{tp+fp}$$,
unde $tp$ reprezintă numărul de instanțe clasificate corect ca aparținând clasei,
iar $fp$ reprezentând numărul de instanțe ale altei clase, clasificate în mod
greșit ca aparținând clasei (erori de tipul I).
Un sistem ce nu atribuie nicio instanță de test în clasa pozitivă va avea $P=1$.

\item {\bf Rapelul}, notat în tabele cu $R$, este dat de $$R = \frac{tp}{tp+fn}$$
unde $tp$ este ca mai sus, iar $fn$ reprezintă numărul de instanțe ale clasei
de interes, clasificate în mod greșit ca neaparținând clasei (erori de tipul II).
Un sistem ce atribuie toate instanțele de test clasei pozitive va avea $R=1$.

\item {\bf Scorul $F_1$} este media armonică între precizie și rapel, evitând
astfel problemele subliniate mai sus prin care se poate obține foarte ușor
un scor mare.  Scorul $F_1$ este potrivit atunci când clasele nu sunt egal
distribuite.
$$F_1 = \frac{2PR}{P+R}$$
\end{itemize}
\subsection{Metrici de etichetare secvențială}
Pentru etichetarea secvențială, ieșirea $y$ are forma unei secvențe de etichete
de instanță.  Scorurile de mai sus se pot calcula la nivelul acestor instanțe.
Pentru o evaluare globală ce ține cont de structura de secvență, se poate folosi
scorul la nivel de item:
$$\mathbf{E}_{i}\left[\mathbf{I}[y^i_p = \hat{y}^i_p\,\forall p]\right]$$
În lucrarea de față ne referim la acest scor ca {\em acuratețe la nivel
de cuvânt} și îl reprezentăm în tabele prin {\em cuv.} sau {\em cuvânt}.
\section{Procesarea limbajului natural}
\label{sec:nlp}
În lucrarea prezentă, aplicațiile abordate sunt din domeniul procesării
limbajului natural și al lingvisticii computaționale.  Secțiunea curentă
conține generalități despre domeniu, și detalii despre sarcinile de interes
a căror rezolvare o încercăm în continuare.

\subsection{Istoria și structura lingvisticii computaționale}
În literatură se obișnuiește ordonarea sarcinilor de procesare a limbajului
natural după {\em nivelul} la care acestea operează.  Altfel spus, cu cât
o sarcină are ca prerechizite alte sarcini, nivelul ei este mai înalt.
Sarcini precum înțelegerea și generarea limbii sunt, discutabil, la cel
mai înalt nivel.  Un exemplu de sarcină dependentă a acestora este detectarea
coreferenței: identificarea lanțurilor de expresii care se referă la
aceeași entitate: {\em acestora} și {\em sarcini precum ...} în
propozițiile anterioare.  Vizibil, pentru rezolvarea unei astfel de sarcini
sunt necesare anumite relații sintactice si semantice, considerate
de nivel și mai jos.  Pe cel mai jos nivel se găsesc sarcini precum
împărțirea unui text în paragrafe și propoziții, și împărțirea unui text
în cuvinte (sarcină remarcabil de dificilă în unele limbi).

Deși o astfel de structură ierarhică arată elegant, ea ascunde multe
subtilități de care cercetătorii se lovesc în practică.  Majoritatea
sarcinilor se află într-un grad mai mare sau mai mic de interdependență.
Multe sisteme de succes precum~\citep{senna} folosesc învățarea concomitentă
a mai multor sarcini.  Folosirea ieșirii unui sistem ca intrare printre
trăsăturile unui alt sistem este o variantă simplă dar eficientă a acestei
idei și o folosim în secțiunea \ref{sec:sil}.

\subsection{Morfologie}
În lingvistică, morfologia este ramura care tratează fenomenele la nivelul
simbolic cel mai mic al limbii.  Majoritatea operațiilor de prelucrare de
text au nevoie să poată face abstracție peste variații de formă ale
cuvintelor, precum diferențele între singular și plural, sau forme diferite
de conjugare ale verbelor.  Dificultatea morfologiei depinde puternic de
limba analizată.  În engleză, de exemplu, problema găsirii rădăcinii unei
forme flexionare date ({\em stemming}) se poate rezolva extrem de eficient
urmând un set de reguli determinat de lingviști \citep{porter}.  Diametral
opus se află unele limbi precum cele din orientul îndepărtat, unde chiar și
găsirea marginilor cuvintelor dintr-un text fluent este o problemă
prea grea pentru a fi abordată cu sisteme construite manual.


\subsubsection{Învățarea și generarea de paradigme}
În limba română, conjugarea verbelor prezintă o dificultate interesantă
sub forma alternanțelor din rădăcină.  Deși, tradițional, verbele au fost
împărțite în patru clase de conjugare, după un model întâlnit frecvent
în limbile latine, variațiile puternice ale paradigmelor verbale din
interiorul claselor au motivat studierea mai profundă a problemei.

Una din figurile de la baza lingvisticii computaționale în limba română,
Grigore C. Moisil, a făcut un prim pas în direcția traducerii automate
\citep{moisil}, propunând un model al alternanțelor din rădăcina verbelor
ca litere variabile.  Rădăcina ar rămâne, astfel, constantă, dar unele
litere din rădăcină pot lua forme diferite în funcție de mod, timp,
număr și persoană.

\citet{gsm} folosesc rețele neurale cu etichete globale la nivel de
cuvânt-formă pentru a învăța să discrimineze între forme foarte regulate
de clase flexionare.  Motivația nu este dezvoltarea unui conjugator, ci
a învățării de reprezentări latente si clustere folosite apoi pentru
justifiarea unor aspecte psiholingvistice ale procesului de învățare
a limbii.

Învățarea structurală în morfologie a fost folosită cu succes în ultimii
ani. \citet{dur} mineaza în mod nesupervizat paradigme morfologice folosind
un model probabilist.  \citet{chang} implementează segmentarea
unităților morfologice din interiorul cuvintelor folosind o metodă similară
cu segmentarea cuvintelor și cu despărțirea în silabe discutată în lucrarea
de față.  În finlandeză, o limbă cu morfologia remarcabil de dificilă, 
\citet{finnish} rezolvă problema generării de forme flexionare necesare în
pasul de decodare al traducerii automate. 

Morfologia substantivelor în limba română a fost abordată prin învățare
automată nesecvențială în \citep{dinusubsta,dinusubstb}.

\subsubsection{Despărțire în silabe și accent}
Despărțirea cuvintelor în silabe are un rol important în generarea și
recunoașterea limbii vorbite, în așezarea documentelor în pagină, și
în poetica computatională.  Deși în multe limbi, printre care si româna,
silabele fonetice și cele ortografice (numite și despărțiri la sfârșit
de rând) nu coincid întotdeauna, corelația este mare și, în absența
a mai multe date, una din ele poate fi folosită cu destul succes
în locul celeilalte.
Identificarea silabei accentuate dintr-un cuvânt este importantă atât
pentru limba vorbită ci și în generarea de limbă scrisă cu constrângeri
metrice, ca în traducerea automată a poeziei \citep{knight}.
Deși nu este o problemă de morfologie pură, ci la granița cu fonetica,
despărțirea în silabe se poate aborda cu metode computaționale ce
folosesc trăsături la nivel de caracter, așa cum vom arăta.
\subsubsection{Segmentarea cuvintelor}
În unele limbi, spre deosebire de română și engleză, delimitarea cuvintelor
nu este explicită, prin spații, ci lăsată în sarcina vorbitorului
nativ cu experiență.  În limbile din orientul îndepărtat, problema
segmentării cuvintelor este fundamentală, din cauza absenței delimitării
și a alfabetelor mai complexe folosite.  Etichetarea secvențială
a fost aplicată cu succes în segmentarea limbei thailandeze \citep{thai},
limbii chineze și limbii japoneze \citep{nakagawa}.

\subsection{Etichetarea părților de vorbire}
În lingvistică se practică împărțirea cuvintelor unei limbi în clase
numite părți de vorbire ({\em substantiv}, {\em adjectiv}, {\em verb}, {\em
pronume}, etc.). În principiu numărul de clase este fix, dar nivelul
de granularitate la care se face analiza poate varia.  Mai jos prezentăm două
exemple de etichetare a părtilor de vorbire într-o propoziție în limba engleză.
$$
\begin{array}{c c c c c c c}
\text{Smith} & \text{is} & \text{not} & \text{in} & \text{his} & \text{office} & \text{.} \\
 NNP & VBZ & RB & IN & PRP\$ &  NN & . \\
 N & V & R & I & P & N & . \\
\end{array}
$$
Pe linia a doua, tagurile pentru $NNP$ (substantiv propriu) și $NN$ (substantiv
comun) sunt reduse la forma comună $N$.  Pentru detalii despre semnificația
etichetelor a se vedea \citep{ptb}. 

În ierarhia lingvistică, problema identificării părții de vorbire a unui
cuvânt se găsește undeva între morfologie și sintaxă.  În unele limbi,
partea de vorbire a unui cuvânt se poate determina cu mai multă eficiență
decât în altele fără să fie nevoie de context și de proprietăti la nivel
de propoziție.  Pentru limbi cu suprapunere mai mare între formele
cuvintelor, sintaxa trebuie luată în seamă în modelarea părților de
vorbire.
\subsection{Chunking}
\label{sec:chunk}
Găsirea de fragmente coerente în interiorul unei propoziții poartă
numele de chunking și este o alternativă robustă la problema
parsării în constituenți.  De exemplu, propoziția
{\em El consideră că deficitul curent al contului se va îngusta până la numai \# 1.8 miliarde în luna septembrie.} se poate grupa ca
[\textsuperscript{NP} El] [\textsuperscript{VP} consideră] că [\textsuperscript{NP} deficitul curent al contului] [\textsuperscript{VP} se va micșora] până la [\textsuperscript{NP} numai \# 1.8 miliarde ] în [\textsuperscript{NP} septembrie ].

O reprezentare de acest fel este extem de utilă în sarcini de nivel mai înalt
în prelucrarea limbajului natural, dar și în îmbunătățirea sarcinilor fundamentale.
De exemplu,~\citet{abney} arată cum se poate obtine un arbore intreg de derivare
plecând de la o segmentare superficială de tip chunking.
