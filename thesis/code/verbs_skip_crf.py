import numpy as np

from sklearn.base import BaseEstimator
from sklearn.feature_extraction import DictVectorizer
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV
from sklearn.externals import joblib

from pystruct.models import ChainCRF, EdgeTypeGraphCRF
from pystruct.learners import OneSlackSSVM

class ChainVectorizer(BaseEstimator):
    def __init__(self, size=2, ngram_size=2):
        self.size = size
        self.ngram_size = ngram_size
        self.vect = DictVectorizer(sparse=False)

    def _build_feature_dict(self, s, position):
        features = {}
        l = len(s)
        for left in xrange(-self.size, self.size + 2):
            for sz in xrange(0, min(self.ngram_size, self.size - left + 1)):
                if position + left >= 0 and position + left + sz + 1 <= l:
                    features['%s|%s' % (left, left + sz)] =\
                        s[position + left:position + left + sz + 1]
        return features

    def fit(self, X, y=None):
        self.vect.fit([self._build_feature_dict(word, k) for word in X
                       for k in xrange(len(word))])
        return self

    def transform(self, X, y=None):
        return np.array([self.vect.transform(
            [self._build_feature_dict(word, k) for k in xrange(len(word))])
            for word in X])


class VerbCRF(ChainCRF, EdgeTypeGraphCRF):
    """CRF with skip edges where the last node is connected to every other one
    """
    def __init__(self, n_states=2, n_features=None, inference_method='qpbo'):
        ChainCRF.__init__(self, n_states, n_features,
                          inference_method=inference_method)
        self.n_edge_types = 2
        self.size_psi = (n_states * self.n_features
                         + self.n_edge_types * n_states ** 2)


    def psi(self, x, y):
        """Feature vector associated with instance (x, y).
        """
        return EdgeTypeGraphCRF.psi(self, x, y)

    def get_edges(self, x, flat=True):
        n = x.shape[0]
        inds = np.arange(x.shape[0])
        A = np.c_[inds[:-1], inds[1:]]
        B = np.c_[inds[:-2], (n - 1) * np.ones((n - 2, 1), dtype=np.int)]
        if flat:
            return np.r_[A, B]
        else:
            return [A, B]

    def get_pairwise_potentials(self, x, w):
        return EdgeTypeGraphCRF.get_pairwise_potentials(self, x, w)


pipe = Pipeline([('vect', ChainVectorizer()),
                 ('clf', StructuredPerceptron(model=ChainCRF, max_iter=50)])

X_train, y_train = joblib.load('train.jbl')
mapping = {'0': 0, 'T1': 1, 'T10': 2, 'T11': 3, 'T12': 4, 'T13': 5, 'T14': 6,
           'T15': 7, 'T16': 8, 'T2': 9, 'T3': 10, 'T4': 11, 'T5': 12, 'T6': 13,
           'T7': 14, 'T8': 15, 'T9': 16, 'a0': 17, 'a1': 18, 'a2': 19, 'a4':
           20, 'c0': 21, 'd0': 22, 'e0': 23, 'e1': 24, 'e2': 25, 'e3': 26,
           'n0': 27, 'o0': 28, 's0': 29, 's1': 30, 't0': 31, 't1': 32, 'u0':
           33}

y_train = [np.array([mapping[i] for i in k]) for k in y_train]
pipe.fit(X_train, y_train)
pipe.score(X_test, y_test)
